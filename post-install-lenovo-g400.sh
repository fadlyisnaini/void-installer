#!/bin/sh

if [ "$(id -u)" = "0" ]; then
	echo "Do not run this script as root" 1>&2
	exit 1
fi

#install_packages() {
#	doas xbps-install void-repo-nonfree xmirror
#	doas xbps-install fish-shell git wget curl chrony alsa-utils ntfs-3g opendoas sv-helper
#	doas xbps-install neovim
#	doas xbps-install base-devel libX11-devel libXft-devel libXinerama-devel
#	doas xbps-install htop cifs-utils plocate udevil atool 7zip-unrar zip xtools
#	doas xbps-install ranger
#	doas xbps-install xorg-minimal
#	doas xbps-install xrdb xset setxkbmap hsetroot xprop xdpyinfo xrandr
#	doas xbps-install font-misc-misc dejavu-fonts-ttf dina-font font-ionicons-ttf terminus-font
#	doas xbps-install -y gnome-themes-standard faenza-icon-theme numix-themes arc-theme xcursor-vanilla-dmz xcursor-vanilla-dmz-aa
#	doas xbps-install -y lxappearance gtk2fontsel xdg-utils xdg-user-dirs xbanish picom dunst brillo
#	doas xbps-install -y rxvt-unicode urxvt-perls
#	doas xbps-install -y mesa-dri libva-intel-driver libva-utils libvdpau-va-gl mesa-vaapi mesa-vdpau
#	doas xbps-install -y firefox libreoffice-writer libreoffice-calc
#	doas xbps-install -y google-fonts-ttf
#	doas xbps-install -y cups epson-inkjet-printer-escpr keepassxc mpv gimp thunderbird crda
#}

install_broadcom_driver() {
	doas xbps-install base-devel
	doas xbps-install broadcom-bt-firmware broadcom-wl-dkms
	doas xbps-install iwd
	doas ln -sf /etc/sv/dbus /etc/runit/runsvdir/default/dbus
	doas ln -sf /etc/sv/iwd /etc/runit/runsvdir/default/iwd
}

install_packages() {
	cur_dir=$(pwd)
	pkgsfile="packages-lenovo-g400.list"

	[ -f "$cur_dir/$pkgsfile" ] && sed -e '/^#/d' -e '/^$/d' "$cur_dir/$pkgsfile" > /tmp/$pkgsfile
	echo "\n>>> Installing packages ... <<<"
	xargs -a /tmp/$pkgsfile doas xbps-install
}

set_default_shell() {
	chsh -s /usr/bin/fish fadly
}

set_keymap() {
	echo "Setting keymap .."
	doas sed -i -e "s|#\?FONT=.*|FONT=Lat2-Terminus16|g" /etc/rc.conf
}

set_locale() {
	echo "Setting locale ..."
	doas sed -i -e "s|LANG=.*|LANG=id_ID.UTF-8|g" /etc/locale.conf
	echo "LC_MESSAGES=en_US.UTF-8" | doas tee -a /etc/locale.conf
	doas sed -i -e "/id_ID.UTF-8/s/^\#//" /etc/default/libc-locales
	echo "Running xbps-reconfigure -f glibc-locales ..."
	doas xbps-reconfigure -f glibc-locales
}

set_timezone() {
	echo "Setting timezone ..."
	doas sed -i -e "s|#HARDWARECLOCK=.*|HARDWARECLOCK=UTC|g" /etc/rc.conf
}

set_staticdns() {
	echo "Setting static dns ..."
	echo | doas tee -a /etc/dhcpcd.conf
	echo "# Static DNS" | doas tee -a /etc/dhcpcd.conf
	echo | doas tee -a /etc/dhcpcd.conf
	echo "# OpenDNS" | doas tee -a /etc/dhcpcd.conf
	echo "static domain_name_servers=208.67.222.222 208.67.220.220" | doas tee -a /etc/dhcpcd.conf
	echo | doas tee -a /etc/dhcpcd.conf
	echo "# Quad9" | doas tee -a /etc/dhcpcd.conf
	echo "#static domain_name_servers=9.9.9.9 149.112.112.112" | doas tee -a /etc/dhcpcd.conf
}

set_logindefs() {
	echo "Setting login.defs ..."
	# login.defs
	echo "ENCRYPT_METHOD          SHA512" | doas tee -a /etc/login.defs
}

set_doasconf() {
	echo "Setting doas.conf ..."
echo "# /etc/doas.conf\n
# See doas.conf(5) for details.\n
\n
# In order to accept all commands\n
permit persist :wheel\n
\n
# Exec command without asking password\n
permit nopass keepenv :wheel cmd loadkeys" | doas tee -a /etc/doas.conf
}

set_chronyconf() {
	echo "Setting chrony.conf ..."
echo "server 0.id.pool.ntp.org iburst
server 1.id.pool.ntp.org iburst
server 2.id.pool.ntp.org iburst
server 3.id.pool.ntp.org iburst
server 0.asia.pool.ntp.org iburst
server 1.asia.pool.ntp.org iburst
server 2.asia.pool.ntp.org iburst
server 3.asia.pool.ntp.org iburst" | doas tee -a /etc/chrony.conf
}

set_service() {
	echo "Disabling services ..."
	# disable some service
	sv-disable agetty-tty4
	sv-disable agetty-tty5
	sv-disable agetty-tty6

	echo "Enabling services ..."
	# enable some service
	sv-enable acpid
	sv-enable alsa
	sv-enable chronyd
}

set_xbps() {
	echo "Setting xbps.conf ..."
	doas cp /usr/share/xbps.d/xbps.conf /etc/xbps.d
	# prevent firefox system extensions extraction
	echo | doas tee -a /etc/xbps.d/xbps.conf
	echo "noextract=/usr/lib/firefox/browser/features/*.xpi" | doas tee -a /etc/xbps.d/xbps.conf
}

set_screen_blanking() {
	echo "Setting screen blanking ..."
echo "\n# Screen blanking
setterm --blank 5" | doas tee -a /etc/rc.local
}

#install_broadcom_driver
install_packages

#set_default_shell
#set_keymap
#set_locale
#set_timezone
#set_staticdns
#set_logindefs
#set_doasconf
#set_chronyconf
#set_service
#set_xbps
#set_screen_blanking
